﻿namespace BareTalk.PKI.Domain.Entities
{
    public class PublicKey : IEntity
    {
        public Guid Id { get; set; } = Guid.Empty;
        public string Base64EncodedKey { get; set; } = "";
        public string OwnerUsername { get; set; } = "";
        public string DisplayName { get; set; } = "";
    }
}