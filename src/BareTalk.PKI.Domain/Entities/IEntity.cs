﻿namespace BareTalk.PKI.Domain.Entities
{
    public interface IEntity
    {
        public Guid Id { get; set; }
    }
}