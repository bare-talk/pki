﻿using BareTalk.PKI.Domain.Entities;

namespace BareTalk.PKI.Domain.Contracts
{
    public interface IPublicKeyService
    {
        Task<bool> AddAsync(PublicKey publicKey, CancellationToken cancellationToken);

        Task<PublicKey?> GetByIdAsync(Guid id, CancellationToken cancellationToken);

        Task<PublicKey?> Claim(Guid id, CancellationToken cancellationToken);

        Task<PublicKey?> ClaimOneFor(string username, CancellationToken cancellationToken);

        Task<IEnumerable<PublicKey>> FindKeysMatchingPartial(string ownerSearch, string me, CancellationToken cancellationToken);
    }
}