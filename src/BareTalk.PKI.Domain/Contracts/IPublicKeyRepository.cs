﻿using BareTalk.PKI.Domain.Entities;

namespace BareTalk.PKI.Domain.Contracts
{
    public interface IPublicKeyRepository
    {
        Task AddAsync(PublicKey publicKey, CancellationToken cancellationToken);

        Task<PublicKey?> GetByIdAsync(Guid id, CancellationToken cancellationToken);

        Task<PublicKey?> GetByOwnerUsernameAsync(string ownerUsername, CancellationToken cancellationToken);

        Task Delete(PublicKey publicKey, CancellationToken cancellationToken);

        Task<IEnumerable<PublicKey>> FindKeysMatching(string ownerSearch, string me, CancellationToken cancellationToken);
    }
}