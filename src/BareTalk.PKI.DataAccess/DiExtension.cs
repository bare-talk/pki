﻿using BareTalk.PKI.DataAccess.Connection;
using BareTalk.PKI.DataAccess.Data;
using BareTalk.PKI.DataAccess.Repository;
using BareTalk.PKI.Domain.Contracts;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace BareTalk.PKI.DataAccess
{
    public static class DiExtension
    {
        public static IServiceCollection AddDataAccess(this IServiceCollection services, ConnectionStringConfiguration connectionStringConfiguration)
        {
            return services
                .AddDbContext<PkiDbContext>(options =>
                {
                    options.UseSqlite(connectionStringConfiguration.ConnectionString, opt =>
                    {
                        opt.MigrationsAssembly(typeof(PkiDbContext).Assembly.ToString());
                    });
                })
                .AddTransient<IPublicKeyRepository, PublicKeyRepository>();
        }
    }
}