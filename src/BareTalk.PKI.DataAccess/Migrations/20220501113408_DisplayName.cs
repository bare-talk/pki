﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BareTalk.PKI.DataAccess.Migrations
{
    public partial class DisplayName : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "DisplayName",
                table: "PublicKeys",
                type: "TEXT",
                nullable: false,
                defaultValue: "");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DisplayName",
                table: "PublicKeys");
        }
    }
}