﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BareTalk.PKI.DataAccess.Migrations
{
    public partial class AddOwnerUsernameAsString : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "OwnerId",
                table: "PublicKeys",
                newName: "OwnerUsername");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "OwnerUsername",
                table: "PublicKeys",
                newName: "OwnerId");
        }
    }
}