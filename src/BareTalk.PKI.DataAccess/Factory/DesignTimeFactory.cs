﻿using BareTalk.PKI.DataAccess.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;

namespace BareTalk.PKI.DataAccess.Factory
{
    internal sealed class DesignTimeFactory : IDesignTimeDbContextFactory<PkiDbContext>
    {
        private const string TargetJsonFile = "design.time.connection.json";
        private const string FileConnectionStringSection = "ConnectionStrings";
        private const string FileConnectionStringSubSection = "Source";
        private const string TargetJsonField = $"{FileConnectionStringSection}:{FileConnectionStringSubSection}";

        public PkiDbContext CreateDbContext(string[] args)
        {
            var configurationbuilder = new ConfigurationBuilder();
            configurationbuilder.AddJsonFile(TargetJsonFile);
            var configuration = configurationbuilder.Build();
            var connectionString = configuration
                .GetSection(TargetJsonField)
                .Value;

            if (connectionString == null)
            {
                throw new ArgumentException($"The connection string was not set. Please set \"{FileConnectionStringSubSection}\" under \"{FileConnectionStringSection}\" in {TargetJsonFile}");
            }

            var dbOptionsBuilder = new DbContextOptionsBuilder();
            dbOptionsBuilder.UseSqlite(connectionString, opt =>
            {
                opt.MigrationsAssembly(typeof(PkiDbContext).Assembly.ToString());
            });

            return new PkiDbContext(dbOptionsBuilder.Options);
        }
    }
}