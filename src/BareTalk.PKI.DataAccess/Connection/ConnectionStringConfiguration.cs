﻿namespace BareTalk.PKI.DataAccess.Connection
{
    public sealed class ConnectionStringConfiguration
    {
        public string ConnectionString { get; set; } = "Data source = pki.db";
    }
}