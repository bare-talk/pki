﻿using BareTalk.PKI.Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace BareTalk.PKI.DataAccess.Data
{
    internal sealed class PkiDbContext : DbContext
    {
        public PkiDbContext(DbContextOptions options) : base(options)
        {
        }

        /// <summary>
        /// Reference nullables for db sets https://docs.microsoft.com/en-us/ef/core/miscellaneous/nullable-reference-types
        /// </summary>
        public DbSet<PublicKey> PublicKeys => Set<PublicKey>();
    }
}