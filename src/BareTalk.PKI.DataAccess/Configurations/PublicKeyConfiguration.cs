﻿using BareTalk.PKI.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace BareTalk.PKI.DataAccess.Configurations
{
    internal sealed class PublicKeyConfiguration : IEntityTypeConfiguration<PublicKey>
    {
        public void Configure(EntityTypeBuilder<PublicKey> builder)
        {
            builder
                .HasKey(k => k.Id);

            builder
                .HasIndex(x => x.Base64EncodedKey)
                .IsUnique(true);

            builder
                .HasIndex(x => x.OwnerUsername);
        }
    }
}