﻿using BareTalk.PKI.DataAccess.Data;
using BareTalk.PKI.Domain.Contracts;
using BareTalk.PKI.Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace BareTalk.PKI.DataAccess.Repository
{
    internal sealed class PublicKeyRepository : IPublicKeyRepository
    {
        private readonly PkiDbContext context;

        public PublicKeyRepository(PkiDbContext context)
        {
            this.context = context;
        }

        public async Task AddAsync(PublicKey publicKey, CancellationToken cancellationToken)
        {
            await context.PublicKeys.AddAsync(publicKey, cancellationToken);
            await context.SaveChangesAsync(cancellationToken);
        }

        public async Task Delete(PublicKey publicKey, CancellationToken cancellationToken)
        {
            context.PublicKeys.Remove(publicKey);
            await context.SaveChangesAsync(cancellationToken);
        }

        public async Task<IEnumerable<PublicKey>> FindKeysMatching(string ownerSearch, string me, CancellationToken cancellationToken)
        {
            return await context.PublicKeys
                .AsNoTracking()
                .Where(key => key.OwnerUsername.Contains(ownerSearch) && key.OwnerUsername != me)
                .GroupBy(key => key.OwnerUsername)
                .Select(key => key.First())
                .ToListAsync(cancellationToken);
        }

        public async Task<PublicKey?> GetByIdAsync(Guid id, CancellationToken cancellationToken)
        {
            return await context.PublicKeys
                .AsNoTracking()
                .FirstOrDefaultAsync(key => key.Id == id, cancellationToken: cancellationToken);
        }

        public async Task<PublicKey?> GetByOwnerUsernameAsync(string ownerUsername, CancellationToken cancellationToken)
        {
            return await context.PublicKeys
                .AsNoTracking()
                .FirstOrDefaultAsync(key => key.OwnerUsername == ownerUsername, cancellationToken: cancellationToken);
        }
    }
}