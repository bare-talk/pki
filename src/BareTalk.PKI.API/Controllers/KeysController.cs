﻿using BareTalk.PKI.API.Models.Error;
using BareTalk.PKI.API.Models.PublicKey;
using BareTalk.PKI.Domain.Contracts;
using BareTalk.PKI.Domain.Entities;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace BareTalk.PKI.Controllers
{
    [ApiController]
    public class KeysController : ControllerBase
    {
        private readonly IPublicKeyService publicKeyService;

        public KeysController(IPublicKeyService publicKeyService)
        {
            this.publicKeyService = publicKeyService;
        }

        [HttpPost("/keys")]
        public async Task<IActionResult> AddNewKey(
            [FromBody] PublicKey publicKey,
            CancellationToken cancellationToken)
        {
            var succeeded = await publicKeyService.AddAsync(publicKey, cancellationToken);

            return succeeded ?
                Ok(new KeyCreatedDto(publicKey.Id)) : BadRequest(new Error { Code = ErrorCode.FailedToAddKey, Message = "The public key was not added." });
        }

        [HttpGet("/keys/{keyId}/claim")]
        public async Task<IActionResult> Claim([FromRoute] Guid keyId, CancellationToken cancellationToken)
        {
            var publicKey = await publicKeyService.Claim(keyId, cancellationToken);

            return publicKey != null ?
                Ok(publicKey) : NotFound(new Error { Code = ErrorCode.NoSuchKeyForGivenOwner, Message = "No public key was available for the given username." });
        }

        [HttpGet("/{ownerUsername}/keys/claim")]
        public async Task<IActionResult> ClaimOneFor([FromRoute] string ownerUsername, CancellationToken cancellationToken)
        {
            var publicKey = await publicKeyService.ClaimOneFor(ownerUsername, cancellationToken);

            return publicKey != null ?
                Ok(publicKey) : NotFound(new Error { Code = ErrorCode.NoSuchKeyForGivenOwner, Message = "No public key was available for the given username." });
        }

        [HttpGet("/keys/exclude/{me}")]
        public async Task<IActionResult> FindKeysMatchingPartial([FromQuery] string usernameQuery, string me, CancellationToken cancellationToken)
        {
            var publicKeys = await publicKeyService.FindKeysMatchingPartial(usernameQuery, me, cancellationToken);

            if (!publicKeys.Any())
            {
                return NotFound(new Error { Code = ErrorCode.NoKeysMatchQuery, Message = "No public keys match the owner username query." });
            }

            var publicKeysDtos = publicKeys
                .Select(k => new PublicKeyRestrictedViewDto(k.Id, k.OwnerUsername, k.DisplayName));

            return Ok(new MatchingPublicKeysDto(publicKeysDtos));
        }
    }
}