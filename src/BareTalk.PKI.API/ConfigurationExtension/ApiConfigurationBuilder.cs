﻿using Microsoft.Extensions.Configuration;
using System;

namespace BareTalk.PKI.API.ConfigurationExtension
{
    public sealed class ApiConfigurationBuilder
    {
        private readonly IConfigurationBuilder configurationBuilder = new ConfigurationBuilder();

        private const string ApplicationEnvironemntVariableName = "ASPNETCORE_ENVIRONMENT";
        private const string Development = "Development";

        private ApiConfigurationBuilder()
        {
        }

        public static ApiConfigurationBuilder Instance()
        {
            return new();
        }

        public ApiConfigurationBuilder AddJsonFiles()
        {
            configurationBuilder.AddJsonFile("appsettings.json");

            var environment = Environment.GetEnvironmentVariable(ApplicationEnvironemntVariableName);
            if (environment != null && environment == Development)
            {
                configurationBuilder.AddJsonFile("appsettings.Development.json");
            }

            return this;
        }

        public IConfigurationRoot BuildConfiguration()
        {
            return configurationBuilder.Build();
        }
    }
}