﻿using System;

namespace BareTalk.PKI.API.Models.PublicKey
{
    internal sealed class KeyCreatedDto
    {
        public KeyCreatedDto(Guid id)
        {
            Id = id;
        }

        public Guid Id { get; }
    }
}