﻿using System.Collections.Generic;

namespace BareTalk.PKI.API.Models.PublicKey
{
    internal sealed class MatchingPublicKeysDto
    {
        public IEnumerable<PublicKeyRestrictedViewDto> MatchingKeys { get; }

        public MatchingPublicKeysDto(IEnumerable<PublicKeyRestrictedViewDto> matchingKeys)
        {
            MatchingKeys = matchingKeys;
        }
    }
}