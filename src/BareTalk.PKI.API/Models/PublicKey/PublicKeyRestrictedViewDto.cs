﻿using System;

namespace BareTalk.PKI.API.Models.PublicKey
{
    internal sealed class PublicKeyRestrictedViewDto
    {
        public PublicKeyRestrictedViewDto(Guid id, string ownerUsername, string displayName)
        {
            Id = id;
            OwnerUsername = ownerUsername;
            DisplayName = displayName;
        }

        public Guid Id { get; }
        public string OwnerUsername { get; }
        public string DisplayName { get; }
    }
}