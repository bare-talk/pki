﻿namespace BareTalk.PKI.API.Models.Error
{
    internal enum ErrorCode
    {
        NoSuchKey = 0,
        NoSuchKeyForGivenOwner = 1,
        FailedToAddKey = 2,
        NoKeysMatchQuery = 3
    }
}