﻿namespace BareTalk.PKI.API.Models.Error
{
    internal sealed class Error
    {
        public ErrorCode Code { get; set; } = 0;
        public string Message { get; set; } = "";
    }
}