using BareTalk.PKI.API.ConfigurationExtension;
using BareTalk.PKI.API.Middleware;
using BareTalk.PKI.Application;
using BareTalk.PKI.DataAccess;
using BareTalk.PKI.DataAccess.Connection;
using BareTalk.PKI.Logging;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;

const string Development = "Development";

var builder = WebApplication.CreateBuilder(args);
builder.Services.AddControllers();

var connectionStringConfiguration = builder.Configuration.Extract<ConnectionStringConfiguration>();

builder.Services.AddDataAccess(connectionStringConfiguration);
builder.Services.AddApplication();
builder.Services.AddTransient<ErrorHandlingMiddleware>();
builder.Host.AddSerilog();

var app = builder.Build();
if (app.Environment.EnvironmentName != Development)
{
    // android emulators required additional setup for TLS, avoid it in development
    app.UseHttpsRedirection();
}
app.UseAuthorization();
app.UseMiddleware<ErrorHandlingMiddleware>();
app.MapControllers();
app.Run();