﻿using Microsoft.Extensions.Hosting;
using Serilog;

namespace BareTalk.PKI.Logging
{
    public static class LogginRegistrationExtension
    {
        public static IHostBuilder AddSerilog(this IHostBuilder services)
        {
            return services
               .UseSerilog((context, loggerConfiguration) =>
               {
                   loggerConfiguration.ReadFrom.Configuration(context.Configuration);
               });
        }
    }
}