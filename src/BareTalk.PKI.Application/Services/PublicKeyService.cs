﻿using BareTalk.PKI.Domain.Contracts;
using BareTalk.PKI.Domain.Entities;
using Microsoft.Extensions.Logging;

namespace BareTalk.PKI.Domain.Services
{
    public class PublicKeyService : IPublicKeyService
    {
        private readonly IPublicKeyRepository publicKeyRepository;
        private readonly ILogger<PublicKeyService> logger;

        public PublicKeyService(IPublicKeyRepository publicKeyRepository, ILogger<PublicKeyService> logger)
        {
            this.publicKeyRepository = publicKeyRepository;
            this.logger = logger;
        }

        public async Task<bool> AddAsync(PublicKey publicKey, CancellationToken cancellationToken)
        {
            try
            {
                await publicKeyRepository.AddAsync(publicKey, cancellationToken);
            }
            catch (Exception exception)
            {
                logger.LogError("Failed to add a new public key. Error: {@Error}", exception.Message);
                return false;
            }

            logger.LogInformation("Successfully added a new public key.");
            return true;
        }

        public async Task<PublicKey?> GetByIdAsync(Guid id, CancellationToken cancellationToken)
        {
            try
            {
                var publicKey = await publicKeyRepository.GetByIdAsync(id, cancellationToken);

                if (publicKey == null)
                {
                    logger.LogInformation("No public key found for ID {@Id}", id.ToString());
                    return null;
                }

                logger.LogInformation("Found a matching public key for ID {@Id}", id.ToString());
                return publicKey;
            }
            catch (Exception exception)
            {
                logger.LogError("Failed to retrieve public key for ID {@Id}. Error: {@Error}", id.ToString(), exception.Message);
                return null;
            }
        }

        public async Task<PublicKey?> Claim(Guid keyId, CancellationToken cancellationToken)
        {
            try
            {
                var publicKey = await publicKeyRepository.GetByIdAsync(keyId, cancellationToken);

                if (publicKey == null)
                {
                    logger.LogInformation("No public key found for username {@Username}", keyId);
                    return null;
                }

                await MarkAsTaken(publicKey, cancellationToken);

                logger.LogInformation("Found a matching public key for username {@Username}", keyId);
                return publicKey;
            }
            catch (Exception exception)
            {
                logger.LogError("Failed to retrieve public key for username {@Username}. Error: {@Error}", keyId, exception.Message);
                return null;
            }
        }

        private async Task MarkAsTaken(PublicKey publicKey, CancellationToken cancellationToken)
        {
            try
            {
                await publicKeyRepository.Delete(publicKey, cancellationToken);
                logger.LogInformation("Successfully removed the public key.");
            }
            catch (Exception exception)
            {
                logger.LogError("Failed to remove the public key {@KeyId}. Error: {@Error}", publicKey.Id, exception.Message);
            }
        }

        public async Task<IEnumerable<PublicKey>> FindKeysMatchingPartial(string ownerQuery, string me, CancellationToken cancellationToken)
        {
            try
            {
                var matchingKeys = await publicKeyRepository.FindKeysMatching(ownerQuery, me, cancellationToken);

                if (matchingKeys.Any())
                {
                    logger.LogInformation("Found some keys for owner query {@Owner}.", ownerQuery);
                    return matchingKeys;
                }
            }
            catch (Exception exception)
            {
                logger.LogError("Failed to find any keys for owner query {@OwnerQuery}. Error: {@Error}", ownerQuery, exception.Message);
            }

            return Array.Empty<PublicKey>();
        }

        public async Task<PublicKey?> ClaimOneFor(string username, CancellationToken cancellationToken)
        {
            var key = await publicKeyRepository.GetByOwnerUsernameAsync(username, cancellationToken);
            if (key == null)
            {
                logger.LogError("Failed to fetch a key for {@User}", username);
                return null;
            }

            await MarkAsTaken(key, cancellationToken);
            logger.LogInformation("Successfully retrieved a key for {@User}", username);
            return key;
        }
    }
}