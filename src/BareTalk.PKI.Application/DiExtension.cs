﻿using BareTalk.PKI.Domain.Contracts;
using BareTalk.PKI.Domain.Services;
using Microsoft.Extensions.DependencyInjection;

namespace BareTalk.PKI.Application
{
    public static class DiExtension
    {
        public static IServiceCollection AddApplication(this IServiceCollection services)
        {
            return services
                .AddTransient<IPublicKeyService, PublicKeyService>();
        }
    }
}